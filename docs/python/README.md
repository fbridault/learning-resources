# Python

Learning Python is very important. This language is the swiss-knife of the
developer. At IRCAD-IHU, we use it for different purposes:

- script, task automation
- prototype, research code
- data analysis
- web server backend
- and deep learning of course !

You can find nice tutorials here:

- [Tutorialspoint](https://www.tutorialspoint.com/python/)
- [learnpython](https://www.learnpython.org/)

A nice list of best practices:

- [GitHub](https://gist.github.com/sloria/7001839)

There are many ways you can work with Python. The command line is the way to
start, but then you may want to learn more about:

- [IPython](https://ipython.org/), for a nicer interactive experience,
- [Jupyter](https://jupyter.org/), to keep track of your experiments using notebooks,
- [Anaconda](https://www.anaconda.com/), a famous Python distribution that makes Python
install easier than ever. This is especially recommended for Windows users,
- [PyCharm](https://www.jetbrains.com/pycharm/), a very good [IDE](https://en.wikipedia.org/wiki/Integrated_development_environment).

