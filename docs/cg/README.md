# Computer Graphics

## OpenGL

If you are not familiar with OpenGL and GPU programming in general, the following website is really a wonderful resource.

- [learnopengl](https://learnopengl.com/)

It is good to learn at least about materials and basic lighting, since this allows
to get the basics about the programmable rendering pipeline.

## Ogre3D

At IRCAD-IHU we use [Ogre3D](https://www.ogre3d.org/). If you are interested
about working on rendering features, you may want to explore the [documentation
and tutorials](https://ogrecave.github.io/ogre/api/latest/)

