# JavaScript

## Videos

- [Let's learn ES6](https://www.youtube.com/playlist?list=PL57atfCFqj2h5fpdZD-doGEIs0NZxeJTX)
- [Fun Fun Function](https://www.youtube.com/channel/UCO1cgjhGzsSYb1rsB4bFe4Q)


## Websites

- [The modern JavaScript tutorial](https://javascript.info/)
- [JavaScript ES6 tutorial](https://codeburst.io/es6-tutorial-for-beginners-5f3c4e7960be)
- [Principles of functional programming in JavaScript](https://flaviocopes.com/javascript-functional-programming/)
- [Functional Programming with JavaScript](https://codeburst.io/functional-programming-in-javascript-e57e7e28c0e5)


