# Nest

> Nest is a framework for building efficient, scalable Node.js server-side applications. It uses progressive JavaScript, is built with TypeScript (preserves compatibility with pure JavaScript) and combines elements of OOP (Object Oriented Programming), FP (Functional Programming), and FRP (Functional Reactive Programming).
Under the hood, Nest makes use of Express, but also provides compatibility with a wide range of other libraries (e.g. Fastify). This allows for easy use of the myriad third-party plugins which are available.

## Documentation

- [Official documentation](https://docs.nestjs.com/)
- [Sources](https://github.com/nestjs/nest)
- [Application samples](https://github.com/nestjs/nest/tree/master/sample)

## Support

- [Official Gitter](https://gitter.im/nestjs/nestjs)
- [Issue tracker](https://github.com/nestjs/nest/issues)