# Web Dev

## Basics: 
1. [JavaScript](javascript)
2. [TypeScript](typescript)
3. [RxJS - Reactive asynchronous programming](rxjs)

### Frontend

- [Angular](angular): Requires solid knowledge in JavaScript/TypeScript & RxJS

### Backend

- [NodeJS](https://blog.risingstack.com/node-hero-tutorial-getting-started-with-node-js/): Requires solid knowledge in JavaScript. When you reach the database tutorial, you may use [this tutorial](https://hackernoon.com/dont-install-postgres-docker-pull-postgres-bee20e200198) to setup a postgresql docker container easily.
- [Nest](nestjs): Requires solid knowledge in JavaScript/TypeScript & RxJS
- [TypeORM](typeorm): Requires solid knowledge in JavaScript/TypeScript & database systems

### DEV OPS

- [Docker](https://docs.docker.com/get-started/): Requires basic knowledge of shell commands and python.