# TypeScript

- [Official Documentation](https://www.typescriptlang.org/docs/home.html)
- [Official Handbook](https://www.typescriptlang.org/docs/handbook/basic-types.html)
- [TS deepdive Gitbook by Basarat Syed](https://basarat.gitbooks.io/typescript/)
- [Object Oriented Programming with TypeScript](http://rachelappel.com/write-object-oriented-javascript-with-typescript/)
- [Functional programming with TypeScript](http://rachelappel.com/write-object-oriented-javascript-with-typescript/)

