# Computer vision

## ITK

ITK is the standard library used to process medical images. 

A full list of ITK examples can be found [here](https://itk.org/Wiki/ITK/Examples). 
And more precisely you can focus on [Image segmentation examples](https://itk.org/Wiki/ITK/Examples#Image_Segmentation),
 as this is more adapted to how we use it.


You can find full guidelines on how to build and how to use ITK here:

- [ItkSoftwareGuide](https://itk.org/ItkSoftwareGuide.pdf)

Their [tutorials](https://itk.org/ITK/help/tutorials.html) look a bit outdated
so this is preferable to use the links above.

## OpenCV

OpenCV is the most famous computer vision library. 
The [official tutorials](https://docs.opencv.org/3.4.0/d9/df8/tutorial_root.html)
are quite good and you can follow them in C++ or Python.

## VTK

The Visualization Toolik (VTK) is world-wide used library used for visualizing medical data 
(and any scientific data for that matter). A majority of our applications use VTK for 3D rendering and data
manipulation.

You can find a full list of VTK examples [here](https://www.vtk.org/Wiki/VTK/Examples/Cxx)

## PCL

The Point Cloud Library contains some of the most used Point cloud registration algorithms as well as 
segmentation and surface reconstruction algorithms.

You can check [these tutorials](http://pointclouds.org/documentation/tutorials/) out, you can find
useful information on building and using different PCL algorithms.

