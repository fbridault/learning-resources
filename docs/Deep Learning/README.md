# Deep Learning Learning Resources

### Maths background online courses
  - [Calculus: Khan academy](https://www.khanacademy.org/math/calculus-1) (Mandatory)
  - [Linear algebra: Khan academy](https://www.khanacademy.org/math/linear-algebra) (Mandatory)
  - [Probability and Statistics: Khan academy](https://www.khanacademy.org/math/statistics-probability) (Mandatory)

### Deep learning online courses
  - [Stanford course: Tensorflow](http://web.stanford.edu/class/cs20si/syllabus.html) (Mandatory)
  - [Fast ai: Pytorch](http://course.fast.ai/) (Mandatory)

### Other useful resources
  - [Awesome Deep Learning: A super collection of deep learning resources](https://github.com/ChristosChristofidis/awesome-deep-learning) (Mandatory)
  - [Most cited deep learning papers](https://github.com/terryum/awesome-deep-learning-papers)
  - [Deep learning reading roadmap](https://github.com/floodsung/Deep-Learning-Papers-Reading-Roadmap)
  - [Deep learning papers to know about in 2018](https://www.kdnuggets.com/2018/03/top-20-deep-learning-papers-2018.html)
  - [25 Open Datasets for Deep Learning Every Data Scientist Must Work With](https://www.analyticsvidhya.com/blog/2018/03/comprehensive-collection-deep-learning-datasets/)
  - [The 9 Deep Learning Papers You Need To Know About (Understanding CNNs)](https://adeshpande3.github.io/The-9-Deep-Learning-Papers-You-Need-To-Know-About.html)
  - [Machine Learning with Python video playlist](https://www.youtube.com/playlist?list=PLQVvvaa0QuDfKTOs3Keq_kaG2P55YRn5v)
  - [13 common mistakes amateur data scientists make](https://www.analyticsvidhya.com/blog/2018/07/13-common-mistakes-aspiring-fresher-data-scientists-make-how-to-avoid-them/)
  - [Google dataset search](https://toolbox.google.com/datasetsearch)
  - [Book: Deep Learning with PyTorch](https://pytorch.org/deep-learning-with-pytorch)
