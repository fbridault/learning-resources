# C++

C++ is the first choice for high performance desktop applications.
The recommended [IDE](https://en.wikipedia.org/wiki/Integrated_development_environment) to work with C++ is [QtCreator](https://www1.qt.io/offline-installers/#section-11). You may of course use other IDE but it has the advantage of being multi-plaftorm. People at IRCAD and IHU use it quite a lot so it
integrates well with our libraries and tools.

Some courses if you are not familiar at all with C++ :

- [Initiation à la programmation C++](https://www.coursera.org/learn/initiation-programmation-cpp) _(French audio, English subtitles)_
- [Programmation orientée objet](https://www.coursera.org/learn/programmation-orientee-objet-cpp) _(French audio, English subtitles)_

If you already know the C++, this could still be interesting that you have a look at this more advanced course, that gives a deeper understanding of low-level concepts, and some insights about post C++11 features :

- [Modern C++](https://www.ipb.uni-bonn.de/teaching/modern-cpp/) _(English)_

Otherwise you can find some simple tutorials [here](http://www.cplusplus.com/doc/tutorial/). 

Last you must keep that page in your bookmarks since you should use will find the very important reference page:

- [cppreference](https://en.cppreference.com/w/)

## CMake

All C++ projects use tools to build the code:

- autotools
- CMake
- Scons
- meson,
- etc...

In our software, we use CMake extensively. So it is important you understand it. I recommend you the 
official [wiki page](https://gitlab.kitware.com/cmake/community/wikis/Home) to start learning CMake. However
you must know that CMake is easy to use for a common usage but hard to master for advanced features. There
are many ways to do the same thing and it is sometimes hard to choose the correct option. So to go further,
the best documentation you can find is to look at big open source projects based on CMake like VTK or OpenCV.
