# Git

It is absolutely essential that you know how to use [Git](https://git-scm.com/).

If you are a new comer at IRCAD/IHU Research and Development Team, then we ask you
to put all the code you will write during your training sessions in one or several git
repositories. It is really important that you take the habit to commit your work
regularly.

So first, if you don't know **Git** yet or if you need to refresh your mind, please have a look 
at [this page](https://try.github.io/).

Then, please login in our [GitLab](https://git.ircad.fr) and create a new repository there to
store your code. We also ask you to create an issue each time you start a new task, and assign it to you. When you have finished, please described with few words what you learned and if necessary the difficulties you faced. Last, fill in the time you spent on the task and close the ticket.

One of the best git client out there is [GitKraken](https://www.gitkraken.com/download),
it is used by most of our developers and it is great visualization tool for git. 
